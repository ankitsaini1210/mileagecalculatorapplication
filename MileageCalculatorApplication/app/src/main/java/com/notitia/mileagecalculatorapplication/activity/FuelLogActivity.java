package com.notitia.mileagecalculatorapplication.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.notitia.mileagecalculatorapplication.R;
import com.notitia.mileagecalculatorapplication.adapter.FuelLogAdapter;
import com.notitia.mileagecalculatorapplication.db.DataBaseHandler;
import com.notitia.mileagecalculatorapplication.utils.Utility;

public class FuelLogActivity extends AppCompatActivity {

    private ListView listView;
    private DataBaseHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_log);
        db = new DataBaseHandler(this);
        initialization();
        setUp();
        setListener();
    }

    private void initialization() {
        listView=(ListView) findViewById(R.id.listView);
    }

    private void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FuelLogAdapter fuelLogAdapter=new FuelLogAdapter(this,db.getAllFuelLog());
        listView.setAdapter(fuelLogAdapter);
    }

    private void setListener() {
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
