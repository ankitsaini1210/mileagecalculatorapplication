package com.notitia.mileagecalculatorapplication.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.notitia.mileagecalculatorapplication.R;
import com.notitia.mileagecalculatorapplication.db.DataBaseHandler;
import com.notitia.mileagecalculatorapplication.model.Mileage;
import com.notitia.mileagecalculatorapplication.utils.Utility;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener {

    private EditText edtVehicleName,edtCurrentReading, edtPreviousReading, edtFuelQuantity, edtFuelPrice;
    private Button btnClear, btnCalculate;
    private LinearLayout llMileageLayout;
    private TextView tvFuelMileage, tvCostMileage;
    private int currentReading, previousReading, distance;
    private double fuelPrice, fuelQuantity, costMileage, fuelMileage;
    private DataBaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DataBaseHandler(this);
        initialization();
        setUp();
        setListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        edtVehicleName.setSelection(0);
    }

    private void initialization() {
        edtVehicleName = (EditText) findViewById(R.id.edtVehicleName);
        edtCurrentReading = (EditText) findViewById(R.id.edtCurrentReading);
        edtPreviousReading = (EditText) findViewById(R.id.edtPreviousReading);
        edtFuelQuantity = (EditText) findViewById(R.id.edtFuelQuantity);
        edtFuelPrice = (EditText) findViewById(R.id.edtFuelPrice);
        btnClear = (Button) findViewById(R.id.btnClear);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);
        llMileageLayout = (LinearLayout) findViewById(R.id.llMileageLayout);
        tvFuelMileage = (TextView) findViewById(R.id.tvFuelMileage);
        tvCostMileage = (TextView) findViewById(R.id.tvCostMileage);
    }

    private void setUp() {

    }

    private void setListener() {
        btnClear.setOnClickListener(this);
        btnCalculate.setOnClickListener(this);
        edtVehicleName.setOnTouchListener(this);
        edtCurrentReading.setOnTouchListener(this);
        edtPreviousReading.setOnTouchListener(this);
        edtFuelQuantity.setOnTouchListener(this);
        edtFuelPrice.setOnTouchListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClear: {
                resetVales();
                break;
            }
            case R.id.btnCalculate: {
                if (!validateCal()) {

                } else if (btnCalculate.getText().toString().equalsIgnoreCase(getResources().getString(R.string.calculate_mileage))) {
                    calculation();
                } else if (btnCalculate.getText().toString().equalsIgnoreCase(getResources().getString(R.string.save))) {
                    Mileage mileage = new Mileage();
                    mileage.setVehicleName(edtVehicleName.getText().toString());
                    mileage.setCurrentReading(currentReading);
                    mileage.setPreviousReading(previousReading);
                    mileage.setDistance(distance);
                    mileage.setFuelMileage(fuelMileage);
                    mileage.setCostMileage(costMileage);
                    mileage.setFuelPrice(fuelPrice);
                    mileage.setFuelQuantity(fuelQuantity);
                    mileage.setDate(Utility.getCurrentDate());
                    db.saveMileage(mileage);
                    resetVales();
                    btnCalculate.setText(getResources().getString(R.string.calculate_mileage));
                    showFuelLog();
                }
                break;
            }
            case R.id.edtCurrentReading:{
                layoutOff();
                break;
            }
            case R.id.edtPreviousReading:{
                layoutOff();
                break;
            }
            case R.id.edtFuelPrice:{
                layoutOff();
                break;
            }
            case R.id.edtFuelQuantity: {
                layoutOff();
                break;
            }
        }
    }

    private void layoutOff(){
        if (btnCalculate.getText().toString().equalsIgnoreCase(getResources().getString(R.string.save))) {
            llMileageLayout.setVisibility(View.GONE);
            btnCalculate.setText(getResources().getString(R.string.calculate_mileage));
        }
    }

    private void calculation() {
        currentReading = Integer.parseInt(edtCurrentReading.getText().toString());
        previousReading = Integer.parseInt(edtPreviousReading.getText().toString());
        if (currentReading > previousReading) {
            llMileageLayout.setVisibility(View.VISIBLE);
            btnCalculate.setText(getResources().getString(R.string.save));
            distance = currentReading - previousReading;
            fuelPrice = Double.parseDouble(edtFuelPrice.getText().toString());
            fuelQuantity = Double.parseDouble(edtFuelQuantity.getText().toString());
            fuelMileage = Utility.getFormattedNumber(distance / fuelQuantity);
            tvFuelMileage.setText(String.valueOf(fuelMileage +" "+ getResources().getString(R.string.km_l)));
            costMileage = Utility.getFormattedNumber(fuelPrice / distance);
            tvCostMileage.setText(String.valueOf(costMileage +" "+ getResources().getString(R.string.inr_km)));

        } else {
            Toast.makeText(this, getResources().getString(R.string.provide_info), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateCal() {
        boolean isSuccess = true;
        if (TextUtils.isEmpty(edtVehicleName.getText().toString())) {
            isSuccess = false;
            Toast.makeText(this, getResources().getString(R.string.vehicle_name_), Toast.LENGTH_SHORT).show();
            return isSuccess;
        }else if (TextUtils.isEmpty(edtCurrentReading.getText().toString())) {
            isSuccess = false;
            Toast.makeText(this, getResources().getString(R.string.current_reading), Toast.LENGTH_SHORT).show();
            return isSuccess;
        } else if (TextUtils.isEmpty(edtPreviousReading.getText().toString())) {
            isSuccess = false;
            Toast.makeText(this, getResources().getString(R.string.previous_reading), Toast.LENGTH_SHORT).show();
            return isSuccess;
        } else if (TextUtils.isEmpty(edtFuelQuantity.getText().toString())) {
            isSuccess = false;
            Toast.makeText(this, getResources().getString(R.string.fuel_quantity_msg), Toast.LENGTH_SHORT).show();
            return isSuccess;
        } else if (TextUtils.isEmpty(edtFuelPrice.getText().toString())) {
            isSuccess = false;
            Toast.makeText(this, getResources().getString(R.string.fuel_prices), Toast.LENGTH_SHORT).show();
            return isSuccess;
        }
        return isSuccess;
    }


    private void resetVales() {
        edtVehicleName.setText("");
        edtCurrentReading.setText("");
        edtPreviousReading.setText("");
        edtFuelQuantity.setText("");
        edtFuelPrice.setText("");
        llMileageLayout.setVisibility(View.GONE);
        btnCalculate.setText(getResources().getString(R.string.calculate_mileage));
    }

    private void showFuelLog() {
        List<Mileage> mileageList = db.getAllFuelLog();
        if (null != mileageList && !mileageList.isEmpty()) {
            Intent intent = new Intent(this, FuelLogActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, getResources().getString(R.string.provide_info_), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.view_fuel_log: {
                showFuelLog();
                return true;
            }
            case R.id.reset: {
                db.resetTable();
                resetVales();
                return true;
            }
            case R.id.about: {
                Intent intent = new Intent(this, AboutUsActivity.class);
                startActivity(intent);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction()==MotionEvent.ACTION_DOWN){
            if (btnCalculate.getText().toString().equalsIgnoreCase(getResources().getString(R.string.save))) {
                llMileageLayout.setVisibility(View.GONE);
                btnCalculate.setText(getResources().getString(R.string.calculate_mileage));
            }
            return false;
        }
        return false;
    }
}
