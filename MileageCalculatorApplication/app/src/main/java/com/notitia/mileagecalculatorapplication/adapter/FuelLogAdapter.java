package com.notitia.mileagecalculatorapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.notitia.mileagecalculatorapplication.R;
import com.notitia.mileagecalculatorapplication.activity.FuelLogActivity;
import com.notitia.mileagecalculatorapplication.model.Mileage;

import java.util.List;

/**
 * Created by ANKIT on 30/07/2017.
 */
public class FuelLogAdapter extends BaseAdapter {

    private Context context;
    private List<Mileage> mileages;
    private LayoutInflater inflater;
    public FuelLogAdapter(Context context, List<Mileage> mileages){
        this.context=context;
        this.mileages=mileages;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return (mileages.size() > 0 ? mileages.size() : 0);
    }

    @Override
    public Mileage getItem(int position) {
        return mileages.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder=null;
        if (null== view){
            view=inflater.inflate(R.layout.adapter_fuel_log,viewGroup,false);
            viewHolder=new ViewHolder();
            viewHolder.tvVehicleName=(TextView)view.findViewById(R.id.tvVehicleName);
            viewHolder.tvDate=(TextView)view.findViewById(R.id.tvDate);
            viewHolder.tvFuelQuantity=(TextView)view.findViewById(R.id.tvFuelQuantity);
            viewHolder.tvTotalFuelPrice=(TextView)view.findViewById(R.id.tvTotalFuelPrice);
            viewHolder.tvFuelMileage=(TextView)view.findViewById(R.id.tvFuelMileage);
            viewHolder.tvCostMileage=(TextView)view.findViewById(R.id.tvCostMileage);
            viewHolder.tvDistance=(TextView)view.findViewById(R.id.tvDistance);
            view.setTag(viewHolder);
        }else {
            viewHolder= (ViewHolder) view.getTag();
        }
        viewHolder.tvDate.setText(getItem(position).getDate());
        viewHolder.tvVehicleName.setText(getItem(position).getVehicleName());
        viewHolder.tvFuelQuantity.setText(getItem(position).getFuelQuantity()+" L");
        viewHolder.tvTotalFuelPrice.setText("INR "+getItem(position).getFuelPrice());
        viewHolder.tvFuelMileage.setText(getItem(position).getFuelMileage()+" KM/L");
        viewHolder.tvCostMileage.setText(getItem(position).getCostMileage()+" INR/KM");
        viewHolder.tvDistance.setText(getItem(position).getDistance()+" KM");
        return view;
    }

    public class ViewHolder{
        private TextView tvVehicleName,tvDate,tvFuelQuantity,tvTotalFuelPrice,tvFuelMileage,tvDistance,tvCostMileage;
    }
}
