package com.notitia.mileagecalculatorapplication.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.notitia.mileagecalculatorapplication.model.Mileage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ANKIT on 29/07/2017.
 */
public class DataBaseHandler extends SQLiteOpenHelper {

    //Database version
    private static final int DATABASE_VERSION = 1;
    // DataBase Name
    private static final String DATABASE_NAME = "mileageManager";
    // Mileage table name
    private static final String TABLE_MILEAGES = "mileages";

    // Mileage Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_VEHICLE_NAME= "vehicleName";
    private static final String KEY_CURRENT_READING = "currentReading";
    private static final String KEY_PREVIOUS_READING = "previousReading";
    private static final String KEY_DISTANCE = "distance";
    private static final String KEY_FUEL_MILEAGE = "fuelMileage";
    private static final String KEY_COST_MILEAGE = "costMileage";
    private static final String KEY_FUEL_PRICE = "fuelPrice";
    private static final String KEY_FUEL_QUANTITY = "fuelQuantity";
    private static final String KEY_DATE = "date";

    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_MILEAGE_TABLE="CREATE TABLE "+TABLE_MILEAGES +"(" +KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_VEHICLE_NAME +" TEXT," +KEY_CURRENT_READING +" TEXT," +KEY_PREVIOUS_READING
                +" TEXT NOT NULL UNIQUE," + KEY_DISTANCE+" TEXT," + KEY_FUEL_MILEAGE +" TEXT," + KEY_COST_MILEAGE +" TEXT,"+ KEY_FUEL_PRICE +" TEXT," + KEY_FUEL_QUANTITY +" TEXT," +KEY_DATE+ " TEXT )";
        sqLiteDatabase.execSQL(CREATE_MILEAGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MILEAGES);
        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public void saveMileage(Mileage mileage){
        SQLiteDatabase db= this.getReadableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_VEHICLE_NAME,mileage.getVehicleName());
        values.put(KEY_CURRENT_READING,mileage.getCurrentReading());
        values.put(KEY_PREVIOUS_READING,mileage.getPreviousReading());
        values.put(KEY_DISTANCE,mileage.getDistance());
        values.put(KEY_FUEL_MILEAGE,mileage.getFuelMileage());
        values.put(KEY_COST_MILEAGE,mileage.getCostMileage());
        values.put(KEY_FUEL_PRICE,mileage.getFuelPrice());
        values.put(KEY_FUEL_QUANTITY,mileage.getFuelQuantity());
        values.put(KEY_DATE,mileage.getDate());
        db.insert(TABLE_MILEAGES,null,values);
        db.close();
    }

    public void resetTable(){
        SQLiteDatabase db= this.getReadableDatabase();
        db.delete(TABLE_MILEAGES, null, null);
    }


    public List<Mileage> getAllFuelLog(){
        List<Mileage> mileageList=new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_MILEAGES +" ORDER BY "+ KEY_ID +" DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Mileage mileage=new Mileage();
                mileage.setId(Integer.parseInt(cursor.getString(0)));
                mileage.setVehicleName(cursor.getString(1));
                mileage.setCurrentReading(Integer.parseInt(cursor.getString(2)));
                mileage.setPreviousReading(Integer.parseInt(cursor.getString(3)));
                mileage.setDistance(Integer.parseInt(cursor.getString(4)));
                mileage.setFuelMileage(Double.parseDouble(cursor.getString(5)));
                mileage.setCostMileage(Double.parseDouble(cursor.getString(6)));
                mileage.setFuelPrice(Double.parseDouble(cursor.getString(7)));
                mileage.setFuelQuantity(Double.parseDouble(cursor.getString(8)));
                mileage.setDate(cursor.getString(9));
                // Adding contact to list
                mileageList.add(mileage);
            } while (cursor.moveToNext());
        }
        return mileageList;
    }
}
