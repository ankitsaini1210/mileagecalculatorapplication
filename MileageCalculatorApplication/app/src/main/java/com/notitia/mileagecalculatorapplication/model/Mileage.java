package com.notitia.mileagecalculatorapplication.model;

/**
 * Created by ANKIT on 29/07/2017.
 */
public class Mileage {
    int _id;
    int currentReading;
    int previousReading;
    int distance;
    double fuelMileage;
    double costMileage;
    double fuelPrice;
    double fuelQuantity;
    String date;
    String vehicleName;

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public int getCurrentReading() {
        return currentReading;
    }

    public void setCurrentReading(int currentReading) {
        this.currentReading = currentReading;
    }

    public int getPreviousReading() {
        return previousReading;
    }

    public void setPreviousReading(int previousReading) {
        this.previousReading = previousReading;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public double getFuelMileage() {
        return fuelMileage;
    }

    public void setFuelMileage(double fuelMileage) {
        this.fuelMileage = fuelMileage;
    }

    public double getCostMileage() {
        return costMileage;
    }

    public void setCostMileage(double costMileage) {
        this.costMileage = costMileage;
    }

    public double getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(double fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
