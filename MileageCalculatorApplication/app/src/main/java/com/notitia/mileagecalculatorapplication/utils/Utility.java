package com.notitia.mileagecalculatorapplication.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ANKIT on 29/07/2017.
 */
public class Utility {


    public static double getFormattedNumber(double value){
        double formattedValue = 0.0;
        try {
            DecimalFormat decimalFormat = new DecimalFormat("##,##,###.##");
            formattedValue = Double.parseDouble(decimalFormat.format(value));
        } catch (Exception e) {
            formattedValue = 0.0;
        }
        return formattedValue;
    }

    public static String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
